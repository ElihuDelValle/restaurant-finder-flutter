import 'package:flutter/material.dart';
import '../screens/meal_screen.dart';
import '../models/meal.dart';

class MealItem extends StatelessWidget {
  final String id;
  final String title;
  final String imageUrl;
  final int duration;
  final Difficulty difficulty;
  final Cost cost;

  const MealItem({
    @required this.id,
    @required this.title,
    @required this.imageUrl,
    @required this.duration,
    @required this.difficulty,
    @required this.cost,
  });

  String get difficultyText {
    switch (difficulty) {
      case Difficulty.Easy:
        return 'Easy';
      case Difficulty.Moderate:
        return 'Moderate';
      case Difficulty.Hard:
        return 'Hard';
      default:
        return 'No Difficulty';
    }
  }

  String get costText {
    switch (cost) {
      case Cost.Cheap:
        return 'Elihu/Amanda';
      case Cost.FarFromCheap:
        return 'Far From Cheap';
      case Cost.NotCheap:
        return 'Not Cheap';
      case Cost.VeryNotCheap:
        return 'Impossible';
      default:
        return 'No Cost';
    }
  }

  void selectMeal(context) {
    Navigator.of(context).pushNamed(MealScreen.route, arguments: id);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => selectMeal(context),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        elevation: 5,
        margin: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(15),
                    topLeft: Radius.circular(15),
                  ),
                  child: Image.network(
                    imageUrl,
                    height: 250,
                    width: double.infinity,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  bottom: 10,
                  right: 10,
                  child: Container(
                    width: 200,
                    color: Colors.black26,
                    child: Text(
                      title,
                      style: TextStyle(fontSize: 22, color: Colors.white),
                      softWrap: true,
                      overflow: TextOverflow.fade,
                    ),
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Icon(Icons.schedule),
                      SizedBox(
                        width: 6,
                      ),
                      Text('$duration min'),
                    ],
                  ),
                  Row(
                    children: [
                      Icon(Icons.work),
                      SizedBox(
                        width: 6,
                      ),
                      Text('$difficultyText'),
                    ],
                  ),
                  Row(
                    children: [
                      Icon(Icons.attach_money),
                      SizedBox(
                        width: 6,
                      ),
                      Text('$costText'),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
