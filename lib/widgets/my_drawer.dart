import 'package:flutter/material.dart';
import 'package:restaurant_finder/screens/filters_screen.dart';
import 'package:restaurant_finder/screens/tabs_screen.dart';

class MyDrawer extends StatelessWidget {
  Widget myListTile(String title, IconData icon, Function onTap) {
    return ListTile(
      leading: Icon(
        icon,
        size: 25,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 15,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: onTap,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(10),
            color: Theme.of(context).primaryColor,
            child: Text(
              "Test",
              style: TextStyle(fontWeight: FontWeight.w900),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          myListTile('Categories', Icons.restaurant, () {
            Navigator.of(context).pushReplacementNamed(TabsScreen.route);
          }),
          myListTile('Filters', Icons.settings, () {
            Navigator.of(context).pushReplacementNamed(FiltersScreen.route);
          })
        ],
      ),
    );
  }
}
