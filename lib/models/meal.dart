import 'package:flutter/foundation.dart';

enum Difficulty { Easy, Moderate, Hard }

enum Cost { Cheap, NotCheap, FarFromCheap, VeryNotCheap }

class Meal {
  final String id;
  final List<String> categories;
  final String title;
  final String imageUrl;
  final List<String> ingredients;
  final List<String> steps;
  final int duration;
  final Difficulty difficulty;
  final Cost cost;
  final bool isGF;
  final bool isLactoseFree;
  final bool isVegeterian;
  final bool isVegan;

  const Meal({
    @required this.id,
    @required this.categories,
    @required this.title,
    @required this.imageUrl,
    @required this.ingredients,
    @required this.steps,
    @required this.duration,
    @required this.difficulty,
    @required this.cost,
    @required this.isGF,
    @required this.isLactoseFree,
    @required this.isVegeterian,
    @required this.isVegan,
  });
}
