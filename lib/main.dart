import 'package:flutter/material.dart';
import 'package:restaurant_finder/dummy_data.dart';
import 'package:restaurant_finder/screens/filters_screen.dart';
import './screens/meal_screen.dart';
import './screens/tabs_screen.dart';
import './screens/category_screen.dart';
import './models/meal.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filters = {
    'glutenFilter': false,
    'vegeterianFilter': false,
    'veganFilter': false,
    'lactoseFilter': false
  };

  List<Meal> _favouriteMeals = [];
  List<Meal> _visibleMeals = DUMMY_MEALS;

  void _setFilters(Map<String, bool> newFilters) {
    setState(() {
      _filters = newFilters;
      _visibleMeals = DUMMY_MEALS.where((meal) {
        if (_filters['glutenFilter'] && !meal.isGF) {
          return false;
        }
        if (_filters['vegeterianFilter'] && !meal.isVegeterian) {
          return false;
        }
        if (_filters['veganFilter'] && !meal.isVegan) {
          return false;
        }
        if (_filters['lactoseFilter'] && !meal.isLactoseFree) {
          return false;
        }
        return true;
      }).toList();
    });
  }

  void _switchFavourite(String mealId) {
    final index = _favouriteMeals.indexWhere((meal) => meal.id == mealId);
    print(index);
    if (index >= 0) {
      setState(() {
        _favouriteMeals.removeAt(index);
      });
    } else {
      setState(() {
        _favouriteMeals
            .add(DUMMY_MEALS.firstWhere((meal) => meal.id == mealId));
      });
    }
  }

  bool isFavourite(String mealId) {
    return _favouriteMeals.any((meal) => meal.id == mealId);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Elihu-Restaurant Finder',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
        accentColor: Colors.greenAccent,
        canvasColor: Color.fromRGBO(217, 236, 235, 1),
        fontFamily: 'Raleway',
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: ThemeData.light().textTheme.copyWith(
              headline6: TextStyle(
                fontFamily: 'RobotoCondensed',
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
              bodyText1: TextStyle(
                color: Color.fromRGBO(21, 23, 23, 1),
              ),
              bodyText2: TextStyle(
                color: Color.fromRGBO(22, 22, 23, 1),
              ),
            ),
      ),
      routes: {
        TabsScreen.route: (ctx) => TabsScreen(_favouriteMeals),
        MealScreen.route: (ctx) => MealScreen(_switchFavourite, isFavourite),
        CategoryScreen.route: (ctx) => CategoryScreen(_visibleMeals),
        FiltersScreen.route: (ctx) =>
            FiltersScreen(saveFilters: _setFilters, filters: _filters)
      },
    );
  }
}
