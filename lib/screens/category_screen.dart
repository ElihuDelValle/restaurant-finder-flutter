import 'package:flutter/material.dart';
import 'package:restaurant_finder/models/meal.dart';
import '../widgets/meal_item.dart';

class CategoryScreen extends StatefulWidget {
  final List<Meal> visibleMeals;

  static const route = '/category';

  CategoryScreen(this.visibleMeals);

  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  String title;
  String id;
  List<Meal> meals;

  @override
  void didChangeDependencies() {
    final args =
        ModalRoute.of(context).settings.arguments as Map<String, String>;

    title = args['title'];
    id = args['id'];
    meals = widget.visibleMeals.where((meal) {
      return meal.categories.contains(id);
    }).toList();
    super.didChangeDependencies();
  }

  void _removeMeal(String mealId) {
    setState(() {
      meals.removeWhere((meal) => meal.id == mealId);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: ListView.builder(
          itemCount: meals.length,
          itemBuilder: (ctx, index) {
            final mealItem = meals[index];
            return MealItem(
              id: mealItem.id,
              title: mealItem.title,
              imageUrl: mealItem.imageUrl,
              cost: mealItem.cost,
              difficulty: mealItem.difficulty,
              duration: mealItem.duration,
            );
          }),
    );
  }
}
