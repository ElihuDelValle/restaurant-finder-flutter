import 'package:flutter/material.dart';

import '../widgets/meal_item.dart';
import '../models/meal.dart';

class FavouritesScreen extends StatelessWidget {
  final List<Meal> favouriteMeals;

  FavouritesScreen(this.favouriteMeals);

  @override
  Widget build(BuildContext context) {
    return favouriteMeals.isEmpty
        ? Center(
            child: Text("No Favourites added"),
          )
        : ListView.builder(
            itemCount: favouriteMeals.length,
            itemBuilder: (ctx, index) {
              final mealItem = favouriteMeals[index];
              return MealItem(
                id: mealItem.id,
                title: mealItem.title,
                imageUrl: mealItem.imageUrl,
                cost: mealItem.cost,
                difficulty: mealItem.difficulty,
                duration: mealItem.duration,
              );
            },
          );
  }
}
