import 'package:flutter/material.dart';
import 'package:restaurant_finder/widgets/my_drawer.dart';

class FiltersScreen extends StatefulWidget {
  final Function saveFilters;
  final Map<String, bool> filters;

  FiltersScreen({this.saveFilters, this.filters});

  static final route = '/filters';

  @override
  _FiltersScreenState createState() => _FiltersScreenState();
}

class _FiltersScreenState extends State<FiltersScreen> {
  var _glutenFreeFilter = false;
  var _veganFilter = false;
  var _vegeterianFilter = false;
  var _lactoseFreeFilter = false;

  @override
  initState() {
    _glutenFreeFilter = widget.filters['glutenFilter'];
    _veganFilter = widget.filters['veganFilter'];
    _vegeterianFilter = widget.filters['vegeterianFilter'];
    _lactoseFreeFilter = widget.filters['lactoseFilter'];
    super.initState();
  }

  Widget _mySwitch(
      {String title, String subtitle, bool switchValue, Function onTap}) {
    return SwitchListTile(
      value: switchValue,
      onChanged: onTap,
      title: Text(
        title,
        style: Theme.of(context).textTheme.headline6,
      ),
      subtitle: Text(subtitle),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Filters"),
        actions: [
          IconButton(
              icon: Icon(Icons.save),
              onPressed: () {
                widget.saveFilters({
                  'glutenFilter': _glutenFreeFilter,
                  'vegeterianFilter': _vegeterianFilter,
                  'veganFilter': _veganFilter,
                  'lactoseFilter': _lactoseFreeFilter,
                });
              })
        ],
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Text(
              "Food filters",
              style: Theme.of(context).textTheme.headline4,
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                _mySwitch(
                  title: "Gluten Free",
                  subtitle: "Find only gluten free meals",
                  switchValue: _glutenFreeFilter,
                  onTap: (newValue) {
                    setState(() {
                      _glutenFreeFilter = newValue;
                      widget.saveFilters({
                        'glutenFilter': _glutenFreeFilter,
                        'vegeterianFilter': _vegeterianFilter,
                        'veganFilter': _veganFilter,
                        'lactoseFilter': _lactoseFreeFilter,
                      });
                    });
                  },
                ),
                _mySwitch(
                  title: "Vegan",
                  subtitle: "Find only vegan meals",
                  switchValue: _veganFilter,
                  onTap: (newValue) {
                    setState(() {
                      _veganFilter = newValue;
                      widget.saveFilters({
                        'glutenFilter': _glutenFreeFilter,
                        'vegeterianFilter': _vegeterianFilter,
                        'veganFilter': _veganFilter,
                        'lactoseFilter': _lactoseFreeFilter,
                      });
                    });
                  },
                ),
                _mySwitch(
                  title: "Vegeterian",
                  subtitle: "find only vegeterian meals",
                  switchValue: _vegeterianFilter,
                  onTap: (newValue) {
                    setState(() {
                      _vegeterianFilter = newValue;
                      widget.saveFilters({
                        'glutenFilter': _glutenFreeFilter,
                        'vegeterianFilter': _vegeterianFilter,
                        'veganFilter': _veganFilter,
                        'lactoseFilter': _lactoseFreeFilter,
                      });
                    });
                  },
                ),
                _mySwitch(
                  title: "Lactose Free",
                  subtitle: "Find only lactose free meals",
                  switchValue: _lactoseFreeFilter,
                  onTap: (newValue) {
                    setState(() {
                      _lactoseFreeFilter = newValue;
                      widget.saveFilters({
                        'glutenFilter': _glutenFreeFilter,
                        'vegeterianFilter': _vegeterianFilter,
                        'veganFilter': _veganFilter,
                        'lactoseFilter': _lactoseFreeFilter,
                      });
                    });
                  },
                )
              ],
            ),
          )
        ],
      ),
      drawer: MyDrawer(),
    );
  }
}
